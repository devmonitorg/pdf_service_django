from django.http import JsonResponse
from django.urls import path, include
from rest_framework import routers

from apps.generator import views as generator_view

app_name = 'api'

ping = path('ping', lambda request: JsonResponse('PONG', safe=False), name="ping")

v1 = [
    (r'pdf', generator_view.PDFView),
]
# Упаковка -- какие объекты входят в версию

routerV1 = routers.DefaultRouter()
for i in v1:
    routerV1.register(*i)
#

urlpatterns = [
    ping,
    path('generator/', include((routerV1.urls, 'v1'), namespace='v1')),
]
