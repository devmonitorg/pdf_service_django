from django.contrib import admin

from apps.generator.models import User, PDF


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('email', )


@admin.register(PDF)
class PDFAdmin(admin.ModelAdmin):
    list_display = ('key', "name", "user", )
