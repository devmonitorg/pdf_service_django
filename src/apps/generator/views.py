import ast
import jinja2
import os
import pdfkit
import logging
import uuid

from bson.objectid import ObjectId
from pymongo import MongoClient
from time import gmtime, strftime

from django.conf import settings
from django.http import Http404
from rest_framework import mixins, generics, status, views
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ViewSetMixin

from apps.generator.models import PDF
from apps.generator.serializer import PDFSerializer, BaseSerializer
from core.settings import PDF_TEMPLATE_PATH, RENDERS_PATH, PROJECT_PATH


client = MongoClient(settings.MONGO_DB_DSN)
db = client["crm_trade"]

logger = logging.getLogger(__name__)


class PDFView(ViewSetMixin, generics.GenericAPIView):
    queryset = PDF.objects.all()
    model = PDF
    serializer_class = PDFSerializer
    lookup_field = "key"

    def get_serializer(self, name):
        templates = {s.__name__.lower(): s for s in BaseSerializer.__subclasses__()}
        return templates.get(name, None)

    @action(detail=False, methods=['post'], url_path='(?P<name>[^/.]+)')
    def generate_view(self, request, name):
        """Под ключу можно получить PDF. Если пользователь владеет этим PDF
        По имени и контексту можно сгенерировать PDF"""
        _serializer = self.get_serializer(name)
        if not _serializer:
            raise Http404

        # Проверить входящие данные, request.data.get('procedure_id'),
        serializer = self.get_serializer(name)(data=request.data)
        serializer.is_valid(raise_exception=True)

        # Вернуть ссылку на шаблон
        return Response(serializer.generate())

    @action(detail=False, methods=['post', 'get'], url_path='generate_bill/(?P<name>[^/.]+)')
    #data={'unique_number': '', 'issued_at': '', 'company_forma': '', 'company_name': '', 'inn': '', 'address': ''}
    def generate_bill(self, request, name):
        inn = request.data.get('inn')
        org_verification = db.org_verification.find_one({'inn': ObjectId(inn)}) if inn else None
        if org_verification:
            data = {
                'issued_at': org_verification["issued_at"],
                'company_forma': org_verification["company_forma"],
                'company_name': org_verification["company_name"],
                'inn': org_verification["inn"],
                'address': org_verification["address"],
            }
        else:
            data = {'issued_at': '', 'company_forma': '', 'company_name': '', 'inn': '',
                    'address': ''}
        data['unique_number'] = strftime("%Y%m%d%H%M%S", gmtime())
        template_path = os.path.join(PROJECT_PATH, PDF_TEMPLATE_PATH, f'{name}.html')
        if os.path.exists(template_path):
            template = jinja2.Template(open(template_path).read().strip())
            render_source = template.render(data)
            key = str(uuid.uuid4())
            render_path = f"{RENDERS_PATH}/{key}"
            render = open(render_path, "w")
            render.writelines(render_source)
            render.close()
            with open(render_path) as render_file:
                pdfkit.from_file(render_file, f'{render.name}.pdf')
            return Response(f'{RENDERS_PATH}/{key}.pdf')
        raise Http404

    def create_static_pdf(self):
        # делаем pdf c данными, которые приходят
        return

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.serializer_class(instance)
        return Response(serializer.data)
