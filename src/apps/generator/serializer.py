import os
import uuid
from pathlib import Path
import jinja2
from bson.objectid import ObjectId
from pymongo import MongoClient
import pdfkit
from django.conf import settings
from django.http import Http404
from rest_framework import serializers

from apps.generator import models
from core.settings import PDF_TEMPLATE_PATH, RENDERS_PATH


client = MongoClient(settings.MONGO_DB_DSN)
db = client["crm_trade"]


class BaseSerializer(serializers.Serializer):
    def fetch_data(self):
        """Получение данных из mongodb"""

        procedure_id = self.request.data.get('procedure_id', None)  # ObjectId data
        self.procedure = db.procedures.find_one({'_id': ObjectId(procedure_id)}) if procedure_id else None
        user_id = self.request.data.get('user_id', None)  # ObjectId data
        self.user = db.users.find_one({'_id': ObjectId(procedure_id)}) if user_id else None

    def generate(self):
        template = jinja2.Template(open(self.template_file()).read().strip())
        render_source = template.render(**self.fetch_data())
        key = str(uuid.uuid4())
        render_path = f"{RENDERS_PATH}/{key}"
        render = open(render_path, "w")
        render.writelines(render_source)
        render.close()
        pdfkit.from_file(render_path, f'{render}.pdf')
        return key

    def template_file(self):
        """Отдать путь на файл шаблона"""
        file = f"{Path(PDF_TEMPLATE_PATH) / PATH(self.base_dir) / self.__class__.__name__.lower()}.html"
        if not os.path.isfile(file):
            raise Http404
        return file


class Message(BaseSerializer):
    base_dir = 'procedures'

    def fetch_data(self):
        super().fetch_data()

        if self.procedure:
            procedure = self.procedure
            owner = db.users.find_one({'_id': procedure['owner']})

            return {
                "procedure": {
                    "owner": {
                        "profile": {
                            "firstname": owner['profile']['firstname'],
                            "lastname": owner['profile']['lastname']
                        }
                    },
                    "data": {
                        "title": procedure['data']['title'],
                        "text": procedure['data']['text']
                    }
                }
            }
        return {}


class Reduction_Report(BaseSerializer):
    base_dir = 'procedures'

    def fetch_data(self, **kwargs):
        return {
            "procedure": {
                "data": {

                }
            }
        }


class Reduction_Report_Partucipant(BaseSerializer):
    base_dir = 'procedures'
    def fetch_data(self):
        return {}


class Trade_Procedure_Report(BaseSerializer):
    base_dir = 'procedures'
    def fetch_data(self):
        return {}


class Trade_Procedure_Report_Participant(BaseSerializer):
    base_dir = 'procedures'
    def fetch_data(self):
        return {}


class Trade_Procedure_Response(BaseSerializer):
    base_dir = 'procedures'
    def fetch_data(self):
        return {}


class Org(BaseSerializer):
    base_dir = 'profile'
    def fetch_data(self):
        return {}


class User(BaseSerializer):
    base_dir = 'profile'
    def fetch_data(self):
        return {}


class Verification(BaseSerializer):
    base_dir = 'profile/'
    def fetch_data(self):
        return {}


class TarifBill(BaseSerializer):
    base_dir = ''
    def fetch_data(self):
        return {
            "bill": {
                "owner": {
                    "profile": {
                        "firstname": "test",
                        "lastname": "test"
                    }
                },
                "data": {
                    "title": "test",
                    "text": "test"
                }
            }
        }


class PDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PDF
        fields = ('context', )

