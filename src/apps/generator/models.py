import logging

import pytz
from django.contrib.postgres.fields import JSONField
from django.db import models

logger = logging.getLogger(__name__)
utc = pytz.UTC


class User(models.Model):
    _id = models.CharField(max_length=255, null=True, unique=True)
    email = models.CharField(max_length=255, null=True, blank=True, unique=True)

    def __str__(self):
        return self.email


class PDF(models.Model):
    key = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    context = JSONField(blank=True, null=True)

    def __str__(self):
        return self.name
