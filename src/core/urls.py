from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include

from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions

from core import settings

schema_view = get_schema_view(
    openapi.Info(
        title="Monitorg payment_service",
        default_version='v1',
    ),
    public=False,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('pdf_generator/admin/', admin.site.urls),
    path("api/1/", include("api", namespace="api")),
    path("healthz/", lambda request: HttpResponse("OK")),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^pdf_generator/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
