FROM python:3.7 AS build-env
ARG templates_dir=/opt/pdf_templates
COPY requirements.pip .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r /requirements.pip


# Собираю инстанс самого проекта
FROM python:3.7-slim as project
RUN apt update
RUN apt install wget -y
RUN apt install tar -y
RUN apt install xz-utils -y
RUN mkdir wkhtmltopdf-download \
 && cd /wkhtmltopdf-download \
 && wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz \
 && ls -a && tar xf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz && rm wkhtmltox-0.12.3_linux-generic-amd64.tar.xz && cp wkhtmltox/bin/wk* /usr/local/bin/

COPY --from=build-env /usr/local/lib/python3.7/site-packages /usr/local/lib/python3.7/site-packages

COPY ./src /opt/application/

WORKDIR /opt/application/

ENV PYTHONPATH /usr/local/lib/python3.7/site-packages
ENV PYTHONPATH /opt/application/
ENV PDF_TEMPLATE_PATH templates
ENV RENDERS_PATH renders

CMD python manage.py runserver 0.0.0.0:8082

