# Registry where you want store your Docker images
DOCKER_REGISTRY = 159.69.125.68:1111/repository/monitorg
PORTS = 8080:8080
TAG = latest
PROJECT_NAME = monitorg-pdf-service

# local
unpack: activate
	poetry install 

activate: venv 
	pip install --user poetry
	poetry env use venv/bin/python

venv:
	python -m venv venv

#test:
#	pytest -vv ${TEST_CASE}

lock:
	poetry lock 

freez: lock
	poetry export -f requirements.txt > requirements.pip

submodules:
	git submodule update --init
# pre production
build: freez
	docker build -t ${DOCKER_REGISTRY}/${PROJECT_NAME}:${TAG} .

run: build
	docker run -it -p ${PORTS} --rm --env-file .env ${DOCKER_REGISTRY}/${PROJECT_NAME}:${TAG}

push: build
	docker push ${DOCKER_REGISTRY}/${PROJECT_NAME}:${TAG}
